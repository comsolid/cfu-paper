# Introdução

O projeto _Cloud Firmware Updating_ \cite{sanusb:2017} do grupo SanUSB é uma ferramenta para
atualização de códigos-fonte a serem executados em equipamentos \iot (_Internet of Things_) através
de um sistema web, onde o usuário cria uma pasta pessoal e envia códigos-fonte,
os quais podem ser _hex_, _shell_, _python_ ou _C/C++_.

No equipamento, que deve ser um sistema embarcado GNU/Linux, deve ser baixado
um _script_ em _shell_ que executa o programa enviado e faz requisições ao sistema
web para baixar o novo código-fonte caso tenha atualizado. Entretanto o sistema
utiliza método de atualização _pull_, ou seja, de tempos em tempos o servidor é requisitado
para verificar se existe atualização do código fonte. Essa técnica aplicada a
equipamentos \iot não é recomendada devido a excessiva
utilização de recursos, tanto de rede quanto de energia.

# Justificativa

Visto que equipamentos embarcados tem recursos limitados é necessário utilizar
protocolos mais leves e que utilizam método _push_ em que o servidor notifica
os equipamentos, como o MQTT que é um protocolo leve de troca de mensagens extremamente
simples que usa _Publish/Subscribe_, projetado para dispositivos embarcados e com
largura de banda baixa, alta latência ou redes não confiáveis. Os objetivos do MQTT
são de minimizar o uso da rede e recursos do dispositivo enquanto tenta assegurar
confiabilidade e algum grau de garantia de entrega nas mensagens \cite{mqtt:1999}.

# Revisão Teórica

Algo semelhante pode ser feito através da ferramenta SWUpdate
(_Software Update for Embedded Systems_) idealizada para permitir a atualização
de sistemas embarcados de forma eficiente e segura. Suporta atualização
local e remota \cite{sbabic:2017}.

Entretanto não aparenta ser uma ferramenta muito didática, ou seja, a aparência
é de uma ferramenta profissional que intimida o iniciante. Nesse sentido o \cfu
tem a vantagem da simplicidade.

# Objetivos

Modificar o aplicativo de verificação em _shell script_ para utilizar o protocolo
MQTT para ser notificado através de aplicativo NodeJS com a biblioteca MQTT.js.

NodeJS é um envólucro (wrapper) do ambiente de execução JavaScript de
alta performance chamado V8 usado no navegador Google Chrome. O NodeJS permite
que o V8 funcione em contextos diferentes do browser, principalmente fornecendo
APIs adicionais que são otimizadas para casos específicos \cite{hughes-croucher:2012}.

Além disso, modificar o sistema web para que ao receber um novo arquivo através
de _upload_ enviar uma notificação ao servidor MQTT, que por sua vez irá notificar
os equipamentos \iot.

# Arquitetura

A arquitetura segue o modelo da Figura \ref{fig:arquitetura}.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.4]{img/arquitetura.png}
	\caption{Arquitetura}
    \label{fig:arquitetura}
\end{figure}

# Considerações Finais e trabalhos futuros

Com a introdução do protocolo \mqtt para notificação do equipamento para que ele
note que existe atualização
a ser baixada, isso irá diminuir bastante o consumo de energia e otimizar o uso da rede.

O próximo passo seria tornar o sistema mais seguro, tanto para que o usuário
possa ter acesso privado ao seu conteúdo quanto para que o conteúdo em si
possa ser armazenado de forma íntegra, principalmente tratando-se se códigos-fonte
que irão ser baixados e executados em ambiente com acesso a internet. Com a
aumento do número de equipamentos ligados a rede aumenta o número de chances
de ataques de _takeover_, ou seja, roubo virtual de IoT's \cite{neal:2017} e \cite{weagle:2016}.






